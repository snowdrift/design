# README

[Snowdrift.coop] website prototype using [Jekyll], [Sass] and [Susy].


## Pages

The prototype includes preliminary versions of the following pages:

- about
- auth/login
- auth/create-account
- auth/reset-passphrase
- auth/verify-account
- dashboard (matches, payment-history, payment-info)
- donate
- how-it-works
- privacy
- snowdrift-project
- sponsors
- terms
- trademarks
- welcome


## TODO / known issues

- make intro video :P
- fix broken links (t-shirts on donate page)

- (Priority: low):
    - create svg logo and use it in nav + mouseover color as link
    - finish creating mimi-eunice css file for easy adding images of the sprite
    - Responsiveness:
        - Add breakpoints/media queries mixin — mixin added, pending breakpoint names to update pages
        - ~~Notifications need adjustment for responsiveness (M&E exceeds container, applied temporary workaround)~~
    - ~~Update font variables across pages due to @font-face changes~~
    - ~~Add asset file path mixin (insert image background property css)~~
    - js-licenses — stub, no libre js yet
    - missing: merchandise — not MVP
    - update layout about — enhance info presentation


## Installation

*Tested on Fedora 23 x86_64.*

- Install Ruby for your distribution

- Install Jekyll: `gem install jekyll bundler`

- Change into the project directory and run `bundle exec jekyll serve` the
  first time, which will fetch additional packages including Susy, if not
  already installed. For subsequent starts, simply do `jekyll serve`.

- View site in the web browser at `http://127.0.0.1:3300`


## Site structure

- `_data` — list of database variables sorted by page
- `_includes` — components of the html default template
- `_layouts` — page layout types
- `_pages` — location of page templates with content, using the `permalink`
  template variable to redirect the pages following the production site urls.
  Pages with a `0-` prefix are prototype-only and do not appear in
  production.
- `_plugins` — contains a few template filters
- `_site` — generated static pages based on templates
- `_static` — site assets including fonts, images and Sass files


## Snippets

- Sass mixins: `_static/css/default/_mixins.sass`
- Liquid filters: `_plugins/filters.rb`

### Sass functions and mixins

- `+bg(image)` — inserts a background declaration. Image path is relative to
  static path in `rt()`.

```
.class
    +bg("icon-login.png")

// Result
.class
    background: url("../img/icon-login.png") center center no-repeat
```
- `+font(size, variant)` — inserts font declarations given font size and
  variant. Variants: l (light), r (regular), m (medium), i (italics)

```
+font(3rem, li)

// Result
font-family: Rubik, sans-serif
font-size: 3rem
font-weight: 300
font-style: italics
```

- `rt(file, type)` — returns the static path for a file depending on type set
  within the function. Route types include: img, font, url

```
rt("logo.png", "img")
// "../img/logo.png"

```

### Liquid filters

- `button` — inserts a button.

```
// {{css-class | button: label}}
{{"newbutton" | button: "Verify"}}
```

- `note` — inserts a notification snippet. Variants: `note_button`,
  `note_form`

```
// note — set last argument to true to include a close button div
{{css-class | note: title, text, bool_close_button [true|false]}}
{{"note-success" | note: "Success!", "You are now registered.", true}}
```

- `note_button` — like `notice`, with a link button instead of a close button

```
{{css-class | note_button: title, text, button label, button url}}
```

- `note_form` — like `note_button`, with a form submit button and argument
  to specify action type. Note that the latter is currently a marker for
  prototyping purposes only, and needs to be hooked up to handlers to
  function.

```
{{css-class | note_form: title, text, button label, action}}
```


## License

Aside from [trademarks], writings and images here are free to use and share
under [CC-BY-SA]. This site's source code is free under the terms of the [GNU
AGPLv3+].


[Snowdrift.coop]: https://snowdrift.coop
[Jekyll]: https://jekyllrb.com/
[Sass]: http://sass-lang.com/
[Susy]: http://susy.oddbird.net/
[trademarks]: https://snowdrift.coop/trademarks
[CC-BY-SA]: https://creativecommons.org/licenses/by-sa/4.0
[GNU AGPLv3+]: https://www.gnu.org/licenses/agpl
