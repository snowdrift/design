---
title: Terms of Use
layout: default
permalink: /terms/
---

<h1 id="snowdrift.coop-terms-of-use">Snowdrift.coop Terms of Use</h1>

<div id="tilt"></div>

<em class="version">Version 0.2 effective February 10, 2014</em>

<h2 id="introduction">Introduction</h2>
<p><strong>We envision a Free, Libre, Open World where everyone has equal access to
non-rival resources of all kinds; where everyone is empowered to realize
and share their additions to our common cultural heritage; where everyone
can freely participate in the ongoing development of science and
technology; and where there is respect for liberty, privacy, and human
dignity for all. —
<a href="{{ site.wiki_url }}/about/mission">Our Vision Statement</a>
</strong>
</p>
<p>Snowdrift.coop (“we” or “us”), is a nonprofit social welfare organization
whose mission is to empower and engage people to create and develop non-rival
resources under a
<a href="free-libre-open">free license</a>
or assigned to the public domain, and to sustainably support Free/Libre/Open
(FLO) projects and affiliated team members. We provide the essential
infrastructure and organizational framework for the long-term development of
<a href="/p">FLO projects</a>
which serve this mission. We strive to ensure that all projects supported
through Snowdrift.coop are consistent with FLO ideals.</p>
<p>We welcome you (“you” or the “user”) to Snowdrift.coop, and we encourage you
to actively engage with the Snowdrift.coop community. Before you participate,
however, we ask that you please read and agree to the following Terms of Use
(“Terms of Use”).</p>
<p><strong>If you are under 13 years old, you may not register an account</strong>
because we must follow the rules set by the Children's Online Privacy
Protection Act. Have a parent or guardian contact us and we can determine
whether there are ways to still allow you to participate.</p>
<h2 id="overview">Overview</h2>
<p>These Terms of Use tell you about our public services at Snowdrift.coop, our
relationship to you as a user, and the rights and responsibilities that guide
us both.</p>
<p>Snowdrift.coop provides a financial conduit from FLO supporters to project
teams but it is NOT a bank or a payment processor in its own right. Your
deposits are not insured against loss or currency fluctuations. You agree to
reimburse us for payment processor fees, charge backs, or reversals. Once you
fund your account and make a
<a href="{{ site.wiki_url }}/about/terminology">pledge</a>,</a>
you authorize us to withdraw funds from your Snowdrift.coop account for the
monthly payout to the project, unless and until you withdraw your pledge. You
may withdraw pledges or cancel your account at any time and may be refunded
your Snowdrift.coop account funds upon cancellation.</p>
<p>Generally we do not monitor content on project sites or individual user
behavior (with the rare exception of violation of policies like these Terms
of Use or the site
<a href="conduct">Code of Conduct</a>,</a>
or legal compliance such as for
<a href="https://en.wikipedia.org/wiki/Digital_Millennium_Copyright_Act">DMCA</a>
notices). This means that standards of behavior are primarily in the hands of
you and the rest of our user community. Your participation is welcome but you
should follow the policies that govern each of the independent projects with
which you interact. Because we are dedicated to making content freely
accessible, we generally require that all content you contribute is available
under a FLO license and that license terms are clearly marked.</p>
<p>Please be aware that you are legally responsible for all of your actions in
connection with Snowdrift.coop projects under the laws of the United States
of America and any other applicable laws (which may include the laws where
you live or use our service). This means it is important that you exercise
caution. In light of this responsibility, we have some behavioral standards
including rules on what you cannot post, most of which protect either you or
other users.</p>
<p>We also include other important notices and disclaimers, so please read these
Terms of Use in their entirety.</p>
<hr />
<h2 id="contents">Contents</h2>
<ol class="toc"><li>Our services</li>
<li>Privacy Policy</li>
<li>Financial transactions</li>
<li>Refraining from certain activities</li>
<li>Licensing of content</li>
<li>DMCA compliance</li>
<li>Management of websites</li>
<li>Disputes and jurisdiction</li>
<li>Disclaimers</li>
<li>Limitation on liability</li>
<li>Modifications to these Terms of Use</li>
<li>Other terms</li>
<li>Thank you!</li>
</ol>
<h2 id="our-services">1. Our services</h2>
<p>Snowdrift.coop is dedicated to encouraging the growth, development, and
distribution of FLO works, and to helping FLO projects maintain a sustainable
economy for their work. Our role is to help the public support these projects
by providing a system for recurring financial transactions to pool funds for
long-term development. However, we act only as a support service, maintaining
the infrastructure and organizational framework that allows our users to
build projects by contributing funds and labor. Because of our unique role,
there are a couple of things you should be aware of concerning our
relationship to you, the projects, and the other users:</p>
<h2 id="privacy-policy">2. Privacy Policy</h2>
<p>We ask that you review the terms of our <a href="privacy">Privacy
Policy</a>, so that you are aware of how we collect and use your
information.</p>
<h2 id="financial-transactions">3. Financial transactions</h2>
<p>Any implemented processor from
<a href="{{ site.wiki_url }}/market-research/payment-services">our list of supported services</a>
can be used to deposit funds to your Snowdrift.coop account. Users may opt in
to automatic monthly contributions if their processor allows or may fund
their account manually. We reserve the right to set a minimum deposit amount.
<strong>Snowdrift.coop will include a charge back for any transaction fees on top
of the deposit amount and will deduct transaction fees from a refund or
withdrawal. Any fees paid to Snowdrift.coop are non-refundable, except as
otherwise expressly stated in these Terms.</strong>
Aside from the patron pledge process where funds go from patron accounts to
projects and from projects to their specified paid team members, there will
be no direct transfers between user accounts.</p>
<p><strong>Once you make a pledge and place funds in your Snowdrift.coop account, you
authorize us to withdraw monthly for each project to which you have pledged
unless and until you withdraw pledges or defund your account.</strong>
To add a new pledge, a patron account must be fully funded for all existing
pledges for 3 months. When users drop below the 3 month buffer, we send a
notice to add funds. When a pledge becomes greater than funds available, if
at least one share is still funded, then the pledge is considered
“underfunded”. If no shares have adequate funds, then the pledge is
considered “unfunded”. Underfunded and unfunded pledges will not count toward
the share value for other supporters. The pledge will be automatically
re-activated when sufficient funds are added to the account. Users may change
their pledges at any time.</p>
<p><strong>We reserve the right to place conditions on withdrawals from accounts with
active pledges.</strong>
Project team members may withdraw funds freely from their accounts up to the
amount they have received from patronage donations, but such withdrawals are
still liable for transaction costs. Upon closing an account, a user may opt
to either receive a refund less transaction costs or to donate the funds
directly to Snowdrift.coop.</p>
<p>Snowdrift.coop reserves the right, at our discretion, to place a hold on
deposits if Snowdrift.coop suspects monies may be subject to charge back,
bank reversal, failure to clear, or fraud. We reserve the right to seek
reimbursement from a user, and the user will reimburse us, if we discover a
fraudulent, erroneous, or duplicate transaction, or if we receive a charge
back or reversal from any user’s payment source or processor for any reason.
<strong>Failure to reimburse a charge back or reversal of payment is cause for
termination of your account.</strong>
</p>
<p><strong>Snowdrift.coop operates in US Dollars and therefore is not responsible for
currency fluctuations</strong>
that occur when billing or crediting a credit or debit card denominated in a
currency other than US Dollars, nor is Snowdrift.coop responsible for
currency fluctuations that occur when receiving or sending payment via a
payment processor.</p>
<p><strong>You expressly acknowledge that (a) Snowdrift.coop is not acting as a
trustee or a fiduciary of the users and that its services are provided to
users administratively; (b) Snowdrift.coop is not a “financial institution”
as defined under the Bank Secrecy Act (BSA) and its services are payment
services rather than banking services; (c) Snowdrift.coop is not a bank and
any payments transferred through Snowdrift.coop are not insured deposits
and are subject to default, loss, or forfeiture.</strong>
</p>
<h2 id="refraining-from-certain-activities">4. Refraining from certain activities</h2>
<p>The projects hosted by Snowdrift.coop exist because of the support of our
vibrant community of users like you. We welcome your participation in this
community and encourage you to be civil and polite in your interactions with
others, to act in good faith, and to make contributions aimed at furthering
our shared mission. For the most detailed, plain English explanations of what
constitutes violating behavior, please refer to our
<a href="{{ site.wiki_url }}/community/conduct">Code of Conduct</a>
and
<a href="{{ site.wiki_url }}/community/honor-users">user Honor Policy</a>.</a>
</p>
<p>The Snowdrift.coop Board of Directors releases official policy resolutions
from time to time. Some of these policies may be mandatory for a particular
project, and, when they are, you agree to abide by them as applicable.</p>
<p>Certain activities, whether legal or illegal, may be harmful to other users
and violate our rules, and some activities may also subject you to liability.
Therefore, for your own protection and for that of other users, you may not
engage in such activities on our sites. These activities include:</p>
<p><strong>Harassing and abusing others</strong>
</p>
<ul><li>Engaging in harassment, threats, stalking, spamming, or vandalism; and</li>
<li>Transmitting chain mail, junk mail, or spam to other users.</li>
</ul>
<p><strong>Violating the privacy of others</strong>
</p>
<ul><li>Infringing the privacy rights of others under the laws of the United
States of America or other applicable laws;</li>
<li>Soliciting personally identifiable information for purposes of
harassment, exploitation, violation of privacy, or any promotional or
commercial purpose not explicitly approved by Snowdrift.coop; and</li>
<li>Soliciting personally identifiable information from anyone under the age
of 13, or from anyone under the age of 18 for an illegal purpose, or
violating any applicable law regarding the health or well-being of
minors.</li>
</ul>
<p><strong>Engaging in false statements, impersonation, or fraud</strong>
</p>
<ul><li>Intentionally or knowingly creating content that constitutes libel or
defamation;</li>
<li>With the intent to deceive, creating content that is false or inaccurate;</li>
<li>Attempting to impersonate another user or individual, misrepresenting
your affiliation with any individual or entity, or using the username of
another user with the intent to deceive; and</li>
<li>Engaging in fraud.</li>
</ul>
<p><strong>Committing infringement</strong>
</p>
<ul><li>Infringing copyrights, trademarks, patents, or other proprietary rights
under applicable law.</li>
</ul>
<p><strong>Misusing our services for other illegal purposes</strong>
</p>
<ul><li>Posting child pornography or any other content that violates applicable
law concerning child pornography;</li>
<li>Posting or trafficking in obscene material that is unlawful under
applicable law; and</li>
<li>Using the services in a manner that is inconsistent with applicable law.</li>
</ul>
<p><strong>Engaging in disruptive and illegal misuse of facilities</strong>
</p>
<ul><li>Creating or distributing content that contains any viruses, malware,
worms, Trojan horses, malicious code, or other device that could harm ourtechnical infrastructure or system or that of our users;</li>
<li>Engaging in uses of the site that are abusive or disruptive of the
services, including placing an undue burden on networks or servers;</li>
<li>Knowingly accessing, tampering with, or using any of our non-public areas
in our computer systems without authorization; and</li>
<li>Probing, scanning, or testing the vulnerability of any of our technical
systems or networks unless all the following conditions are met:
<ul><li>such actions do not unduly abuse or disrupt our technical systems or
networks;</li>
<li>such actions are not for personal gain (except for attribution for
your work);</li>
<li>you report any vulnerabilities to our developers (or fix them
yourself); and</li>
<li>you do not undertake such actions with malicious or destructive
intent.</li>
</ul>
</li>
</ul>
<p>We reserve the right to exercise our enforcement discretion with respect to
the above terms.</p>
<h2 id="licensing-of-content">5. Licensing of content</h2>
<p>As per our mission to grow the commons of free knowledge and free culture,
all users contributing to projects are required to grant general permissions
to re-distribute and re-use their contributions freely, so long as that use
is properly attributed and the same freedom to re-use and re-distribute is
granted to any derivative works.</p>
<p>You agree to the following licensing requirements:</p>
<ol><li><strong>Content</strong>:</strong>
<ul><li>When you submit to Snowdrift.coop any text to which you hold the
copyright, you agree to license it under the
<a href="https://creativecommons.org/licenses/by-sa/43.0/">Creative Commons Attribution-ShareAlike 4.0 International License (“CC BY-SA”)</a>
unless you specify in writing that you prefer to license with the
<a href="https://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License (“CC BY”)</a>
or to waive your copyright via the
<a href="https://creativecommons.org/publicdomain/zero/1.0/">CC0 Universal Public Domain Dedication</a>.</a>
</li>
<li>You may import content that you have found elsewhere or that you have
co-authored with others, but you must warrant that such content is
available under terms that are compatible with the CC BY-SA 4.0
license, and you must follow the terms (such as indicating
attribution) per the license of the content.</li>
<li>Non-text media within the projects listed at Snowdrift.coop are
available under a variety of different Snowdrift.coop-approved
licenses. When you contribute non-text media to a project, you agree
to comply with our requirements for such licenses, as well as with
any requirements of the specific project.</li>
<li>Please note that these licenses allow commercial uses of your
contributions, as long as such uses are compliant with the other
terms.</li>
</ul>
</li>
<li><strong>Trademarks</strong>:</strong>
All Snowdrift.coop trademarks belong to Snowdrift.coop, and any use of
our trade names, trademarks, service marks, logos, or domain names must
be in compliance with these Terms of Use and in compliance with our
<a href="trademark">Trademark Policy</a>.</a>
</li>
</ol>
<h2 id="dmca-compliance">6. DMCA compliance</h2>
<p>Snowdrift.coop wants to ensure that the content that we support can be
re-used by others without fear of liability or infringement. <strong>In order
to provide that assurance, our policy is to respond to notices compliant with
the Digital Millennium Copyright Act (DMCA).</strong> Pursuant to the DMCA,
we will terminate, in appropriate circumstances, users of our system and
network who are repeat infringers.</p>
<p>However, we also recognize that not every takedown notice is valid or in good
faith. We strongly encourage users to have a defense plan prepared if their
content involves material used under a copyright exemption
(<a href="http://www.teachingcopyright.org/handout/fair-use-faq">Teaching Copyright</a>
can help you determine if an exemption applies), and to file
counter-notifications when they appropriately believe a DMCA notice is
invalid or improper. For more information on what to do if you believe a DMCA
notice to be improper, you may consult the
<a href="www.chillingeffects.org">Chilling Effects website</a>.</a>
</p>
<p>If you are the owner of content that is being used on one of the projects
without your permission, you may request that the content be removed under
the DMCA. To make such a request, please contact us
<a href="/about">through the site</a>
or snail mail our designated agent.</p>
<p>Alternatively, any user with copyright concerns about specific content may
make an investigation request to our community, which often handles copyright
issues faster and more effectively than prescribed under the DMCA. We also
have more information available on
<a href="{{ site.wiki_url }}/community/honor-violations">how the community handles violations</a>.</a>
</p>
<h2 id="management-of-websites">7. Management of websites</h2>
<p>The community has the primary role in creating and enforcing policies and
Snowdrift.coop rarely intervenes in project-internal decisions. In an unusual
case, the need may arise or the community may ask us to address a user who
has engaged in significant project disturbance or dangerous behavior. In such
cases, we reserve the right, but do not have the obligation, to:</p>
<ul><li>Investigate your use of the service (a) to determine whether a violation
of these Terms of Use, project policy, or other applicable law or policy
has occurred, or (b) to comply with any applicable law, legal process, or
appropriate governmental request;</li>
<li>Detect, prevent, or otherwise address fraud, security, or technical
issues or respond to user support requests;</li>
<li>Refuse, disable, or restrict access to the content of any user who
violates these Terms of Use;</li>
<li>Ban a user from editing or contributing or block a user's account or
access for actions violating these Terms of Use, including repeat
copyright infringement;</li>
<li>Take legal action against users who violate these Terms of Use (including
reports to law enforcement authorities); and</li>
<li>Manage otherwise the project websites in a manner designed to facilitate
their proper functioning and protect the rights, property, and safety of
ourselves and our users, licensors, partners, and the public.</li>
</ul>
<p>If a user has had their account or access blocked under this provision, they
are prohibited from creating or using another Snowdrift.coop account or
otherwise seeking access to that project, unless we provide explicit
permission. Snowdrift.coop will not ban a user from donating or contributing
or block a user's account or access solely because of good faith criticism
that does not result in actions otherwise violating these Terms of Use or
community policies.</p>
<p>The Snowdrift.coop community and its members may also take action when so
allowed by Honor Code or Bylaws policies applicable to the specific project,
including but not limited to warning, investigating, blocking, or banning
users who violate those policies. You agree to comply with the final
decisions of dispute resolution bodies that are established by the community
for the specific project (such as arbitration committees); these decisions
may include sanctions as set out by the policy of the specific project.</p>
<p>If your account or access is blocked or otherwise terminated, your public
contributions will remain publicly available (subject to applicable
policies), and, unless we notify you otherwise, you may still access our
public pages for the sole purpose of using public materials. In such
circumstances, however, you may not be able to access your account or
settings.</p>
<p>We reserve the right to suspend or end services at any time, with or without
cause, and with or without notice. Even after your use and participation are
banned, blocked or otherwise suspended, these Terms of Use will remain in
effect with respect to relevant provisions.</p>
<div class="disputes-and-jurisdiction"><h2 id="disputes-and-jurisdiction">8. Disputes and jurisdiction</h2>
<p><em>Highlighted for emphasis</em>
</p>
<p>In the event there is a dispute, we encourage you to seek resolution
through the dispute resolution procedures or mechanisms provided by the
projects and Snowdrift.coop. If you seek to file a legal claim against us,
you agree to file and resolve it exclusively in a state or federal court
located in Washtenaw County, Michigan. You also agree that the laws of the
State of Michigan and, to the extent applicable, the laws of the United
States of America will govern these Terms of Use, as well as any legal
claim that might arise between you and us (without reference to conflict of
laws principles). You agree to submit to the personal jurisdiction of, and
agree that venue is proper in, the courts located in Washtenaw County,
Michigan, in any legal action or proceeding relating to us or these Terms
of Use.</p>
<p><strong>Any material downloaded or otherwise obtained through your use of our
services is done at your own discretion and risk.</strong>
You will be solely responsible for any damage to your computer system or
loss of data that results from the download of any such material. You agree
that we have no responsibility or liability for the deletion of, or the
failure to store or to transmit, any content or communication maintained by
the service. We retain the right to create limits on use and storage at our
sole discretion at any time with or without notice.</p>
<p><em>Some states or jurisdictions do not allow the types of disclaimers in this
section, so they may not apply to you either in part or in full depending
on the law.</em>
</p>
<h2 id="disclaimers">9. Disclaimers</h2>
<p><em>Highlighted for emphasis</em></p>
<p><strong>At Snowdrift.coop, we do our best to provide a reliable connection
    between projects and supporters, but your use of our services is at your
    sole risk.</strong> We provide these services on an “as is” and “as
available” basis, and we expressly disclaim all express or implied warranties
of all kinds, including but not limited to the implied warranties of
merchantability, fitness for a particular purpose, and non-infringement. We
make no warranty that our services will meet your requirements, be safe,
secure, uninterrupted, timely, accurate, or error-free, or that your
information will be secure.</p>
<p><strong>Our content is for general informational purposes only.</strong>
Although we host information that pertains to legal or financial issues, this
content should not be taken as professional advice. Please seek independent
professional counseling from someone who is licensed or qualified in the
applicable area in lieu of acting on any information, opinion, or advice
contained in one of the project websites.</p>
<p><strong>You may encounter some material that you find offensive, erroneous,
    misleading, mislabeled, or otherwise objectionable.</strong> We therefore
ask that you use common sense and proper judgment when using our services and
that you report abusive content through the proper channels to the project or
to Snowdrift.coop.</p>
<p><strong>We are not responsible for the content, data, or actions of third
    parties.</strong> You release us, our directors, officers, employees, and
agents from any claims and damages, known and unknown, arising out of or in any
way connected with any claim you have against any such third parties. No advice
or information, whether oral or written, obtained by you from us or through or
from our services creates any warranty not expressly stated in these Terms of
Use.</p>
<p><strong>Any material downloaded or otherwise obtained through your use of
    our services is done at your own discretion and risk.</strong> You will be
solely responsible for any damage to your computer system or loss of data that
results from the download of any such material. You agree that we have no
responsibility or liability for the deletion of, or the failure to store or to
transmit, any content or communication maintained by the service. We retain the
right to create limits on use and storage at our sole discretion at any time
with or without notice.</p>
<p><em>Some states or jurisdictions do not allow the types of disclaimers in
this section, so they may not apply to you either in part or in full depending
on the law.</em></p>
<h2 id="limitation-on-liability">10. Limitation on liability</h2>
<p><em>Highlighted for emphasis</em>
</p>
<p>Snowdrift.coop will not be liable to you or to any other party for any
direct, indirect, incidental, special, consequential or exemplary damages,
including but not limited to, damages for loss of profits, goodwill, use,
data, or other intangible losses, regardless of whether we were advised of
the possibility of such damage. In no event shall our liability exceed one
thousand U.S. dollars (USD 1000.00) in aggregate. In the case that
applicable law may not allow the limitation or exclusion of liability or
incidental or consequential damages, the above limitation or exclusion may
not apply to you, although our liability will be limited to the fullest
extent permitted by applicable law.</p>
</div>
<h2 id="modifications-to-these-terms-of-use">11. Modifications to these Terms of Use</h2>
<p>Just as the Snowdrift.coop community's input is essential for the growth and
maintenance of the projects, we believe that community input is essential for
these Terms of Use to properly serve our users. It is also essential for a
fair contract.</p>
<p>Therefore, while we are in the process of incorporation and beta testing, we
welcome input from users on these interim Terms of Use. However, the Terms
will be subject to change without advance notice. We will provide <em>ex post
facto</em> notice to users via the homepage of the site or via email whenever
our interim Terms of Use (marked with a 0.x version number) change.</p>
<p>Once we reach stable operating status, our permanent Terms of Use (marked
with a 1.x or higher version number), as well as any substantial future
revisions of such permanent Terms of Use, will be submitted to the community
for comment at least thirty (30) days before implementation. For changes for
legal or administrative reasons, to correct an inaccurate statement, or
changes in response to community comments, we will provide at least three (3)
days' notice. We will provide notice of such modifications and the
opportunity to comment via our homepage and via email update. However, we ask
that you please periodically review the current version of these Terms of
Use.</p>
<p>Your continued use of our services after the new Terms of Use become official
following the notice and review period constitutes an acceptance of these
Terms of Use on your part. For the protection of Snowdrift.coop and other
users like yourself, if you do not agree with our Terms of Use, you cannot
use our services.</p>
<h2 id="other-terms">12. Other terms</h2>
<p>These Terms of Use do not create an employment, agency, partnership, or joint
venture relationship between you and us, Snowdrift.coop. You understand that,
unless otherwise agreed to in writing by us, you have no expectation of
compensation for any activity, contribution, or idea that you provide to us,
the community, or a project. In the case of any conflict between these Terms
of Use and any separate signed written agreements between you and us, the
signed agreements will control. These Terms of Use and additional written
terms supersede any unwritten agreements.</p>
<p>You agree that we may provide you with notices, including those regarding
changes to the Terms of Use, by email, regular mail, or postings on our
website.</p>
<p>If in any circumstance, we do not apply or enforce any provision of these
Terms of Use, it is not a waiver of that provision.</p>
<p>These Terms of Use were written in English (U.S.). In the event of any
differences in meaning between the original English version and a
translation, the original English version takes precedence.</p>
<p>If any provision or part of a provision of these Terms of Use is found
unlawful, void, or unenforceable, that provision or part of the provision is
deemed severable from these Terms of Use and will be enforced to the maximum
extent permissible, and all other provisions of these Terms of Use will
remain in full force and effect.</p>
<h2 id="thank-you">13. Thank you!</h2>
<p>We appreciate your taking the time to read these Terms of Use, and we are
very happy to have you using our services. Through your support, you are
helping to build something really big – not only allowing FLO projects to
flourish, but also strengthening a vibrant community of like-minded and
engaged peers, focused on a noble goal.</p>
<hr />
<p><em>These Terms of Use were adapted from the
<a href="https://wikimediafoundation.org/wiki/Terms_of_Use">Wikimedia Terms of Use</a>
which are made available by the
<a href="https://wikimediafoundation.org/wiki/Home">Wikimedia Foundation, Inc.</a>
under the terms of the
<a href="https://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution/Share-Alike license (CC-BY-SA) v3.0</a>.</a>
The Snowdrift.coop Terms of Use are made available by us in their entirety
under
<a href="https://creativecommons.org/licenses/by-sa/4.0/">v4.0 of the same CC-BY-SA license</a>.</a>
</em>
</p>
</div>
</div>
