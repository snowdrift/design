# A collection of common UI elements

module Jekyll
    module UIElements
        # Backend parts ------------------------------------------------------

        # This would normally be a template, added for consolidation
        def loginfields(i)
            token = ""
            email = "Email address"
            pass = "Passphrase"
            "<input type=\"hidden\" name=\"_token\" value=\"#{token}\">
            <div class=\"required\">
            <input id=\"hident2\" name=\"f1\" type=\"text\" required \
            value=\"\" autofocus=\"\" autocomplete=\"email\" placeholder=\"#{email}\"></div>
            <div class=\"required\">
            <input id=\"hident3\" name=\"f2\" type=\"password\" required \
            value=\"\" placeholder=\"#{pass}\"></div>"
        end

        def tokenfield(i)
            token = ""
            label = "Token"
            "<input type=\"hidden\" name=\"_token\" value=\"#{token}\">
            <div class=\"required\"><label for=\"hident2\">#{label}</label>
            <input id=\"hident2\" name=\"f1\" type=\"text\" required \
            value=\"\" autofocus=\"true\"></div>"
        end

        # Notification like note_button, but for settings
        # that require db write, e.g. unmatch button
        def note_form(cl, title, text, label, action)
            "<form class=\"#{cl}\" method=\"post\">
                <h2>#{title}</h2>
                <p>#{text}</p>
                <button class=\"button\" type=\"submit\">#{label}</button>
            </form>"
            # In production, maybe call handler here based on action type
        end


        # Reusable parts -----------------------------------------------------

        # Given css class and button label, insert button
        def button(cl, label)
            "<button class=\"#{cl}\" type=\"submit\">#{label}</button>"
        end
        def bigbutton(label)
            button("bigbutton", label)
        end

        # Given class, title, text and whether there is a close button.
        # insert notification
        def note(cl, title, text, has_close)
            x = ""
            if has_close
                x = "<a href=\"#\" class=\"close\">x</a>"
            end
            "<div class=\"#{cl}\">#{x}
                <h2>#{title}</h2>
                <p>#{text}</p>
            </div>"
        end

        # Given css class, title, text, button label and button url,
        # insert notification with a button
        def note_button(cl, title, text, label, url)
            "<div class=\"#{cl}\">
                <h2>#{title}</h2>
                <p>#{text}</p>
                <a href=\"#{url}\">#{label}</a>
            </div>"
        end


    end
end

Liquid::Template.register_filter(Jekyll::UIElements)
