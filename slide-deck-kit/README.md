# Slide Deck Kit

## Contents

- template.odp — LibreOffice Impress sample file.

- snowdrift.soc — design guide colour palette for LibreOffice. See the Design Guide in the design docs for a preview.

- sample-images/ — sample illustrations to use in slides. More Mimi & Eunice images, including the original comic strips, are available in the `/mimi-and-eunice` section of the design repo. If you cannot find a suitable image, please  ask for assistance.

## Fonts

- Font family: Nunito, available in the design repo
- Suggested settings:
    - Text colour: dark blue or other blue
    - Heading 1: 36 pt
    - Heading 2: 28 pt
    - Paragraph: 16 pt
