<a href="https://snowdrift.coop" target="blank"><img src="/logo/export/snowdrift-logo_symbol-wordmark_dark-blue.svg.png" height="80px" alt="Snowdrift.coop" /></a>

# Moved

This repository is deprecated. **The design library has moved [here](https://seafile.test.snowdrift.coop/d/6861aa006f4e441da72a/).**


## Design Repository

Official [Snowdrift.coop] design files.


## Credits

- snowdrift-dilemma illustration by [hanitles]
- [Mimi & Eunice] comics and characters originally by Nina Paley
- website mockups prior to summer 2015 by Charles Allen, Taylor Keckler, Cody Johnson and Nathan Laughlin


## License

Unless otherwise noted, the contents of this repository are made available under the  [CC-BY-SA 4.0] license.  
The Snowdrift.coop logo and trademarks are governed under the [trademark policy].


[Snowdrift.coop]: https://snowdrift.coop/
[hanitles]: https://github.com/hanitles/illustration
[Mimi & Eunice]: https://wiki.snowdrift.coop/communications/mimi-eunice
[CC-BY-SA 4.0]: https://creativecommons.org/licenses/by-sa/4.0/
[trademark policy]: https://snowdrift.coop/trademarks
