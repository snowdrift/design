#!/bin/sh
# Description: a script to package Ghost theme
# Date: 2017-05-14

OUTPUT="/change/this/to/your/work/dir"
ASSETS="$OUTPUT/snowdrift/assets"
SASSC="/usr/bin/sassc"

# Compile sass to css
echo "Compiling Sass ..."
cd $ASSETS
$SASSC -t compressed sass/screen.sass > css/screen.css

# Package theme
echo "Packing theme ..."
cd $OUTPUT
rm -rf snowdrift.zip
zip -qr9 snowdrift.zip snowdrift/*

echo "Done."
