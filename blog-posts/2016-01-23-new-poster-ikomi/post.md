# New Poster

Hi everyone, I'm a designer and FLO software enthusiast recently joining the design team. I mostly work with Robert and other volunteers — with feedback from other teams — to create illustrations and design documents for the site.  
  
Today's update begins with a warm welcome to our new volunteer, [Anita](https://snowdrift.coop/u/972)! She's been a wonderful addition to the team and with her help, we've finished banners and posters just in time for [SCALE 2016](https://www.socallinuxexpo.org/scale/14x). 

***

## A work-FLO from sketch to print

Prior to the initial sketches, we spent some time discussing the [Snowdrift Dilemma](https://snowdrift.coop/p/snowdrift/w/en/snowdrift) and the key issues we want to highlight in an illustration. If roads reflected the current state of public goods and sustainability, what would it be like to travel along them?

During the discussion, Anita made a series of pencil drawings. Once we had a sketch everyone liked overall, she scanned and imported the sketch into [Inkscape](http://inkscape.org). To convert the sketch to vector graphics, we make an outline of shapes and then block in some colors to get an overall view of the weight and positioning of various elements. Gradually, the characteristics of the two paths became more distinct to underscore the contrast between the two choices.

![](https://snowdrift.coop/img/illustration-roads1-sketch)
![](https://snowdrift.coop/img/illustration-roads2-color)

Using the swatches feature in Inkscape, we converted the colors to align with our design guide to ensure the illustration will look at home with other print materials and on the website. It took multiple passes to bring out important elements like the road and desaturate colors that were too bright. In later stages, we incorporated further stylistic cues like rounded corners and an additional tone on the trees to give them a more rounded look. [Nina Paley](http://ninapaley.com/)'s expressive [Mimi & Eunice](http://mimiandeunice.com/) characters make an appearance, offering a glimpse of hope despite the gloomy status quo that perhaps *something* can be done.
   
![](https://snowdrift.coop/img/illustration-roads3-palette)

For print-ready files, we exported a high-resolution bitmap image and imported it into [Krita](http://krita.org/) for CMYK color space color adjustment and conversion. Here's the completed poster, with some further adjustments and addition of the Snowdrift.coop logo:

![](https://snowdrift.coop/img/poster-snowdrift-dilemma)

## What's next?

The poster will debut as a helpful illustration for Bryan and Aaron (and other volunteers) to use at the Snowdrift.coop booth this weekend at SCALE. It makes it super easy to describe the issues with proprietary projects by just pointing at the illustration.

In the coming weeks, we'll continue illustrating other FLO concepts and make inroads on the website UI and templates. We'll also be solidifying our process documentation to make it even easier for new members to jump in and help with various aspects of design. If the latter sounds like something you enjoy doing — while getting to meet other friendly people passionate about FLO — we'd love to hear from you! Drop by our IRC channel at [#snowdrift](https://snowdrift.coop/p/snowdrift/w/irc) or subscribe to our [design email list](http://lists.snowdrift.coop/mailman/listinfo/design). If drawing wiggly lines isn't your thing, we welcome [other ways to help](https://snowdrift.coop/p/snowdrift/w/en/how-to-help) too. Thanks for your support! 
